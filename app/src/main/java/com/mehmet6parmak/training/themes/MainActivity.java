package com.mehmet6parmak.training.themes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onViewStylesClicked(View view) {
        startActivity(new Intent(this, ViewStylesActivity.class));
    }

    public void onWindowStylingClicked(View view) {
        startActivity(new Intent(this, WindowStylingActivity.class));
    }

    public void onWidgetStylingClicked(View view) {
        startActivity(new Intent(this, WidgetStylingActivity.class));
    }

    public void onMultipleThemesClicked(View view) {
        startActivity(new Intent(this, MultipleThemeActivity.class));
    }
}
