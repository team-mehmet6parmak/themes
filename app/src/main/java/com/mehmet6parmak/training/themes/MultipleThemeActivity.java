package com.mehmet6parmak.training.themes;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RadioGroup;

public class MultipleThemeActivity extends AppCompatActivity {

    private static final String KEY_THEME = "theme";
    private static final int THEME_LIGHT = 0;
    private static final int THEME_DARK = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int theme = getPreferences(Context.MODE_PRIVATE).getInt(KEY_THEME, THEME_LIGHT);
        if (theme == THEME_LIGHT) {
            setTheme(R.style.AppTheme_WidgetStyling);
        } else if (theme == THEME_DARK) {
            setTheme(R.style.AppTheme_WidgetStyling_Dark);
        }
        setContentView(R.layout.activity_multiple_theme);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RadioGroup grp = (RadioGroup) findViewById(R.id.radio_group_theme);
        grp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int style = THEME_LIGHT;
                if (checkedId == R.id.radio_dark_theme) {
                    style = THEME_DARK;
                }
                getPreferences(Context.MODE_PRIVATE).edit().putInt(KEY_THEME, style).commit();
                startActivity(new Intent(MultipleThemeActivity.this, MultipleThemeActivity.class));
                finish();
            }
        });
    }
}
